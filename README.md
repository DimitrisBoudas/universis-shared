# Universis-Shared

This project contains packages which are essentially autonomous modules to be shared with other `@universis/*` applications.

## List of available packages/modules

### Graduation:  
Contains the components/services for implementing the creation of Graduation request in `@universis/students` and viewing its progress in `@universis/registrar`.
