import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { GraduationRoutingModule } from './graduation.router';
import { StudentService } from './services/student/student.service';
import { GraduationRequestService } from './services/graduation-request/graduation-request.service';
import { GraduationRequestComponent } from './components/graduation-request/graduation-request.component';
import { GraduationRequestFormComponent } from './components/graduation-request-form/graduation-request-form.component';
import { GraduationRequirementsCheckComponent } from './components/graduation-requirements-check/graduation-requirements-check.component';
import {
  GraduationDocumentSubmissionComponent
} from './components/graduation-document-submission/graduation-document-submission.component';
import { GraduationCeremonyComponent } from './components/graduation-ceremony/graduation-ceremony.component';
import { GraduationRequestWizardDirective } from './directives/graduation-request-step-directive';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    TranslateModule,
    GraduationRoutingModule
  ],
  providers: [
    StudentService,
    GraduationRequestService,
    TranslateService
  ],
  declarations: [
    GraduationRequestComponent,
    GraduationRequestFormComponent,
    GraduationRequirementsCheckComponent,
    GraduationDocumentSubmissionComponent,
    GraduationCeremonyComponent,
    GraduationRequestWizardDirective
  ],
  entryComponents: [
    GraduationRequestFormComponent,
    GraduationRequirementsCheckComponent,
    GraduationDocumentSubmissionComponent,
    GraduationCeremonyComponent
  ]
})
export class GraduationModule { }
