import { TestBed } from '@angular/core/testing';
import { GraduationRequestService } from './graduation-request.service';
import { ProfileService } from './../../profile/services/profile.service';

describe('GraduationRequestService', () => {
  const profileSvc = jasmine.createSpyObj('ProfileService', ['getStudent']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: ProfileService,
          useValue: profileSvc
        }
      ]
    })
    .compileComponents();
  });

  it('should be created', () => {
    const service: GraduationRequestService = TestBed.get(GraduationRequestService);
    expect(service).toBeTruthy();
  });
});
