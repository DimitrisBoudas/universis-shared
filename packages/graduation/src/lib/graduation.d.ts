/**
 *
 * Type definitions for the graduation module
 *
 */

import { GraduationRequestStep } from "./graduation-request-step";


/**
 *
 * Possible values for graduation item statuses
 * Is used for graduation request steps, graduation rules and document submissions
 * @enum {string}
 * @readonly
 *
 * @property {string} Pending The item is be processed by other entity than the user.
 * @property {string} Completed The item is completed successfully
 * @property {string} Failed The process that the item resemble is failed or completed with errors
 * @property {string} Unavailable The item is not available to the user yet
 *
 */
export enum GraduationItemStatus {
  Pending = 'pending',
  Completed = 'completed',
  Failed = 'failed',
  Unavailable = 'unavailable',
}

/**
 *
 * Possible values for a graduation rule
 * @enum {string}
 * @readonly
 *
 * @property {string} Simple A standalone requirement rule
 * @property {string} LogicOr Has children rule and they are combined with the 'or' operator
 * @property {string} LogicAnd Has children rule and they are combined with the 'and' operator
 *
 */
export enum GraduationRequirementType {
  Simple = 'simple',
  LogicOr = 'or',
  LoginAnd = 'and',
}

/**
 *
 * Graduation requirements a simple or nested items which resemble rules that the
 * student must fulfil in order to be able to graduate.
 * These rules can be nested and be combined with operators against each other
 * @interface
 *
 * @property {string} title The graduation requirement title
 * @property {string} description The description of th requirement
 * @property {GraduationRequirementType} type Requirements can be simple or used with operators
 * @property {GraduationItemStatus} status The current status of the rule for a given student
 * @property {Array<GraduationRequirement>} [subrules] Child rules for the current rule
 *
 */
export interface GraduationRequirement {
  title: string;
  description: string;
  type: GraduationRequirementType;
  status: GraduationItemStatus;
  subrules?: Array<GraduationRequirement>;
}

/**
 * 
 * A step of the graduation request wizard
 * 
 * @property {string} title The title of the step
 * @property {string} alternateName The alternate name of the step
 * @property {string} status The status of the step
 * @property {GraduationRequestStep} component The component to use for the step
 * 
 */
export interface GraduationRequestStepEntry {
  title: string;
  alternateName: string;
  status: string;
  component: GraduationRequestStep;
}

/**
 *
 * An overview of the graduation request process
 * @interface
 *
 * @property {boolean} graduationPeriodExist Indicates whether there is a current or future graduation period
 * @property {Date} requestPeriodStart The start of the graduation period
 * @property {Date} requestPeriodEnc The end of the graduation period
 * @property {any} academicPeriod The academic period information as fetch from the API
 * @property {boolean} graduationRequestPeriodOpen Indicates whether the department graduation period is open
 * @property {string} graduationRequestStatus The status of the actual graduation request
 * @property {string} prerequisitesCheck The status of the prerequisites check
 * @property {string} documentSubmission The status of the document submission
 * @property {string} graduationCeremony The status of the graduation ceremony
 *
 */
export interface GraduationRequestStatus {
  graduationPeriodExist: boolean;
  academicPeriod: any;
  academicYear: string;
  requestPeriodStart: Date;
  requestPeriodEnd: Date;
  graduationRequestPeriodOpen: boolean;
  graduationRequestStatus: string;
  prerequisitesCheck: string;
  documentSubmission: string;
  graduationCeremony: string;
  steps: Array<GraduationRequestStepEntry>;
}
