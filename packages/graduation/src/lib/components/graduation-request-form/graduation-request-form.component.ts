import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'lib-graduation-request-form',
  templateUrl: './graduation-request-form.component.html'
})
export class GraduationRequestFormComponent implements OnInit {

  /**
   *
   * The graduation request special request form
   *
   */
  @ViewChild('studentGraduationRequest') studentGraduationRequest: NgForm;

  /**
   *
   * States whether the student has already submitted the form
   *
   */
  public submitted: boolean;

  /**
   *
   * Dictates if a graduation request can be submitted
   *
   */
  public canSubmit = false;

  /**
   *
   * A list of items that the student can consent to
   *
   */
  public graduationRequestConsents = [
    {
      id: 'participateInCeremony',
      translationKey: 'UniversisGraduationModule.GraduationRequest.ParticipateInCeremony',
      required: true
    },
    {
      id: 'requirementsChecked',
      translationKey: 'UniversisGraduationModule.GraduationRequest.RequirementsChecked',
      required: true
    }
  ];

  /**
   *
   * The status of the checkboxes
   *
   */
  public consents = new Array(this.graduationRequestConsents.length).fill(false);

  /**
   *
   * The message that the request contains. This message is completed by the application
   *
   */
  public requestMessage = 'UniversisGraduationModule.GraduationRequestDefaultRequestMessage';

  /**
   *
   * A message that the student can send alongside their graduation request
   *
   */
  public specialRequest: string;


  // TODO:Make it variable
  public studentRequest = {
    specialRequest: '',
    participateInCeremony: false,
    requirementsChecked: false
  }

  constructor() { }

  ngOnInit() {
  }

  /**
   *
   * Updates the status of a checkbox and decides if the form can be submitted
   *
   * @param event The HTML event that triggered the status change
   * @param index The index of the checkbox to update it's status
   *
   */
  updateConsentsStatus(event: any, index: number): void {
    this.consents[index] = event.target.checked;

    this.canSubmit = this.graduationRequestConsents.reduce((aggregator, consentItem, i) => {
      if (consentItem.required) {
        return aggregator && this.consents[i];
      } else {
        return aggregator;
      }
    }, true);
  }

  /**
   * submits the form
   */
  async submit() {
    this.submitted = !this.submitted;
    console.log('Fake form submission');
  }

}
