import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationRequestFormComponent } from './graduation-request-form.component';

describe('GraduationRequestFormComponent', () => {
  let component: GraduationRequestFormComponent;
  let fixture: ComponentFixture<GraduationRequestFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationRequestFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationRequestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
