import { Component, OnInit } from '@angular/core';
import { LocalizedDatePipe } from '@universis/common';

@Component({
  selector: 'lib-graduation-ceremony',
  templateUrl: './graduation-ceremony.component.html'
})
export class GraduationCeremonyComponent implements OnInit {

  public certificates = [
    {
      title: 'Πιστοποιητικό Lorem Ipsum 1',
      certificateUrl: 'https://dummy.url',
    },
    {
      title: 'Πιστοποιητικό Lorem Ipsum 2',
      certificateUrl: 'https://dummy.url',
    },
    {
      title: 'Πιστοποιητικό Lorem Ipsum 3',
      certificateUrl: 'https://dummy.url',
    },
  ];

  public graduationStatuses = {
    unannounced: {
      alternateName: 'unannounced',
      icon: '',
      message: 'Η προσέλευσή σου στην ορκωμοσία του τμήματος είναι υποχρεωτική.'
    },
    announced: {
      alternateName: 'announced',
      icon: '',
      message: 'Η προσέλευσή σου στην ορκωμοσία του τμήματος είναι υποχρεωτική.'
    },
    attended: {
      alternateName: 'attended',
      icon: 'fas fa-graduation-cap',
      message: 'Συγχαρητήρια! Είσαι πλέον πτυχιούχος του τμήματος Ψυχολογίας Α.Π.Θ.'
    },
    absent: {
      alternateName: 'absent',
      icon: '',
      message: 'Δυστυχώς, δεν προσήλθες στην ορκωμοσία σου. Μπορείς να επαναλάβεις την αίτησή σου το επόμενο εξάμηνο.'
    },
  };

  public graduationDate = new Date();


  public status = this.graduationStatuses.unannounced;
  // public graduationDate = new Date();
  public graduationLocation = 'Αίθουσα Τελετών ΑΠΘ';

  constructor() { }

  ngOnInit() {
  }
  // TODO: Write downloadFile function
  downloadCertificate() {

  }
}
