# Graduation

This package (in the form of a shared module) contains the required shared components and services to be used by `@universis/students` and `@universis/registrar` to deliver the graduation requests feature.
